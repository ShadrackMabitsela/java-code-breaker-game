/*-----------------------------------------------------------------------------#
# Program:     Mastermind [Java].                                              #
# Author:      Shadrack Mabitsela [smabitse@student.wethinkcode.co.za].        #
# Created:     03 / 03 / 2021.                                                 #
# Updated:     04 / 03 / 2021  15:22.                                          #
#                                                                              #
# Description: This program generates a random 4 digit code for the            #
#              player to break. The player is given 12 turns to try and        #
#              break/solve the code.                                           #
#              The program contains three classes:                             #
#              Mastermind.java, CodeGenerator.java and Player.java.            #
#-----------------------------------------------------------------------------*/
package za.co.wethinkcode.mastermind;

/**
 * <p><b><i><u>MASTERMIND CLASS</u></i></b></p>
 * As the Mastermind class contains the main() method of the program,
 * the Mastermind class is the entry point of the program.<br/>
 * <b>The Mastermind class contains the following methods:</b>
 * <dl>
 *   <dt><b>main</b>(String[] args)</dt>
 *   <dd>-> The entry point and it calls the run() method.</dd>
 *   <dt><b>runGame</b>()</dt>
 *   <dd>-> Runs the mastermind programs game loop.</dd>
 *   <dt><b>checkCodeCorrectness</b>(String playerCode, String masterCode)</dt>
 *   <dd>-> Checks the accuracy of the players code and provides feedback.</dd>
 * </dl>
 *
 * @author  Shadrack Mabitsela
 * @version 1.0
 * @since   2021-03-02
 */
public class Mastermind {
    private final String code;
    private final Player player;

    /**
     * The Mastermind class's overloaded constructor.
     * @param generator Type: CodeGenerator [User Defined Class].
     * @param player Type: Player [User Defined Class].
     */
    public Mastermind(CodeGenerator generator, Player player) {
        this.code = generator.generateCode();
        this.player = player;
    }

    /**
     * The Mastermind class's default constructor.
     */
    public Mastermind(){
        this(new CodeGenerator(), new Player());
    }

    /**
     * This method runs the mastermind programs game loop.
     */
    public void runGame() {
        System.out.println("4-digit Code has been set. Digits in range 1 to 8" +
                ". You have 12 turns to break it.");

        while (true) {
            String guessResponse = player.getGuess();
            if (guessResponse.equals("quit") || guessResponse.equals("exit")) {
                System.out.println("The code was: " + this.code + ".\nBye!");
                break;
            }
            if (guessResponse.length() == 4) {
                String correctnessMsg =
                        checkCodeCorrectness(guessResponse, this.code);
                if (correctnessMsg.contains("Congratulations!")) {
//                    System.out.println(correctnessMsg);
                    break;
                }
                if (correctnessMsg.contains("No more turns left.")) {
//                    System.out.println(correctnessMsg);
                    break;
                }
//                System.out.println(correctnessMsg);
            }
        }
    }

    /**
     * This method checks the accuracy of the players code and provides
     * feedback.
     * @param playerCode Type: String.
     * @param masterCode Type: String.
     * @return String This method returns string feedback for the player.
     */
    public String checkCodeCorrectness(String playerCode, String masterCode) {
        var response = new StringBuilder();
        int correctDigitsAndPosition = 0, correctDigitsOnly = 0;

        for (int i = 0; i < 4; i++)
            if (playerCode.charAt(i) == masterCode.charAt(i))
                correctDigitsAndPosition++;
            else if (masterCode.contains(String.valueOf(playerCode.charAt(i))))
                correctDigitsOnly++;

        System.out.println("Number of correct digits in correct place: " +
                correctDigitsAndPosition);
        System.out.println("Number of correct digits not in correct place: " +
                correctDigitsOnly);

        if (correctDigitsAndPosition == 4) {
            System.out.println("Congratulations! You are a codebreaker!");
            System.out.println("The code was: " + masterCode);
            response.append("Congratulations!");
        }
        else {
            player.loseATurn();
            if (player.getPlayerTurns() != 0)
                System.out.println("Turns left: " + player.getPlayerTurns());
        }

        if (player.getPlayerTurns() == 0) {
            System.out.println("No more turns left.");
            System.out.println("The code was: " + masterCode);
            response.append("No more turns left.!");
        }
        return response.toString();
    }

    /**
     * The programs entry point and it calls the run() method.
     * @param args Type: String array.
     */
    public static void main(String[] args) {
        Mastermind game = new Mastermind();
        game.runGame();
    }
}

package za.co.wethinkcode.mastermind;

import java.util.Random;

/**
 * <p><b><i><u>CODEGENERATOR CLASS</u></i></b></p>
 * The CodeGenerator class generates the random 4-digit long code
 * for the player to break/solve.<br/>
 * <b>The CodeGenerator class contains the following methods:</b>
 * <dl>
 *   <dt><b>generateCode</b>()</dt>
 *   <dd>-> Generates the random 4-digit code, where each digit is in the
 *   range 1 to 8 only.</dd>
 * </dl>
 *
 * @author  Shadrack Mabitsela
 * @version 1.0
 * @since   2021-03-02
 */
public class CodeGenerator {
    private final Random random;

    /**
     * The CodeGenerator class's default constructor.
     */
    public CodeGenerator(){
        this.random = new Random();
    }

    /**
     * The CodeGenerator class's overloaded constructor.
     * @param random Type: Random.
     */
    public CodeGenerator(Random random){
        this.random = random;
    }

    /**
     * This method generates a random 4 digit code, using this.random, where
     * each digit is in the range 1 to 8 only.
     * Duplicated digits are allowed.
     * @return String Returns the generated 4-digit code.
     */
    public String generateCode() {
        var min = 1;
        var max = 8;
        var code = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            var digit = random.nextInt(max) + min;
            code.append(digit);
        }
        // Remember code is type: StringBuilder not String.
        // Convert StringBuilder to String.
        return code.toString();
    }
}

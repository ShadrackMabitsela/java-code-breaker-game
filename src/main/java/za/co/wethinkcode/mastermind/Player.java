package za.co.wethinkcode.mastermind;

import java.io.InputStream;
import java.util.Scanner;

/**
 * <p><b><i><u>PLAYER CLASS</u></i></b></p>
 * The Player is responsible for everything related to the player. It keeps
 * track of the players info e.g: number of turns player input etc.<br/>
 * <b>The Player class contains the following methods:</b>
 * <dl>
 *   <dt><b>getPlayerTurns</b>()</dt>
 *   <dd>-> Gets the number of turns the player has.</dd>
 *   <dt><b>hasNoTurns</b>()</dt>
 *   <dd>-> checks the number of turns left.</dd>
 *   <dt><b>loseATurn</b>()</dt>
 *   <dd>-> Decrements the total number of the players turns by one.</dd>
 *   <dt><b>verifyGuess</b>()</dt>
 *   <dd>-> Checks the validity of the players guess.</dd>
 *   <dt><b>getGuess</b>()</dt>
 *   <dd>-> Gets input from the user via the terminal/console.</dd>
 *   <dt><b>playerWantsToQuit</b>()</dt>
 *   <dd>-> Checks if the player wants to quit the mastermind game.</dd>
 * </dl>
 *
 * @author  Shadrack Mabitsela
 * @version 1.0
 * @since   2021-03-02
 */
public class Player {
    private final Scanner inputScanner;
    private boolean quit = false;
    private int turns = 12;

    /**
     * The Player class's default constructor.
     */
    public Player(){
        this.inputScanner = new Scanner(System.in);
    }

    /**
     * The Player class's overloaded constructor.
     * @param inputStream Type: InputStream.
     */
    public Player(InputStream inputStream){
        this.inputScanner = new Scanner(inputStream);
    }

    /**
     * This method gets the number of turns the player has.
     * @return int Returns the number of turns the player has.
     */
    public int getPlayerTurns() {
        return this.turns;
    }

    /**
     * This method checks the number of turns left.
     * @return boolean Returns a boolean value based on the number of turns.
     */
    public boolean hasNoTurns() {
        return this.getPlayerTurns() == 0;
    }

    /**
     * This method decrements the total number of the players turns by one.
     */
    public void loseATurn() {
        if (!this.hasNoTurns()) { this.turns--; }
    }

    /**
     * This method checks the validity of the players guess.
     * @param guess Type: String.
     * @return String Returns a string response based on the validity of the
     * player's guess.
     */
    public String verifyGuess(String guess) {
        var tempGuess = new StringBuilder();
        tempGuess.append(guess);

        var lengthOfFour = false;
        var allDigits = false;
        var one_to_8 = false;

        lengthOfFour = tempGuess.length() <= 4;

        if (tempGuess.length() == 4) {
            for (int i = 0; i < 4; i++) {
                if (Character.isDigit(tempGuess.charAt(i)))
                    allDigits = true;
                else {
                    allDigits = false;
                    break;
                }
            }

            for (int i = 0; i < 4; i++) {
                int digit = Character.getNumericValue(tempGuess.charAt(i));
                if (digit >= 1 && digit <= 8)
                    one_to_8 = true;
                else {
                    one_to_8 = false;
                    break;
                }
            }
        }
        if (!lengthOfFour || !allDigits || !one_to_8)
            return "badCode";
        else
            return guess;
    }

    /**
     * This method gets input from the user via the terminal/console.
     * This must prompt the user to re-enter a guess until a valid 4-digit
     * string is entered, or until the user enters `exit` or `quit`.
     * @return String The value entered by the user.
     */
    public String getGuess() {
        while (true) {
            System.out.println("Input 4 digit code:");
            String playerGuess = inputScanner.nextLine().trim()
                    .replaceAll("\\s", "");

            this.quit = playerGuess.equalsIgnoreCase("quit") ||
                    playerGuess.equalsIgnoreCase("exit");

            if (playerGuess.equalsIgnoreCase("quit"))
                return "quit";
            if (playerGuess.equalsIgnoreCase("exit"))
                return "exit";

            playerGuess = verifyGuess(playerGuess);
            if (playerGuess.equals("badCode")) {
                System.out.println("Please enter exactly 4 digits (each " +
                        "from 1 to 8).");
                continue;
            }
            return playerGuess;
        }
    }

    /**
     * This method checks if the player wants to quit the mastermind game.
     * @return boolean Returns the boolean value of the private field 'quit'.
     */
    public boolean playerWantsToQuit() {
        return this.quit;
    }
}

package za.co.wethinkcode.mastermind;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class CodeGeneratorTest {
    @Test
    public void codeHasLengthOf4() {
        var codeGen = new CodeGenerator();
        var generatedCode = codeGen.generateCode();
        assertEquals(4, generatedCode.length());
    }

    @Test
    public void codeHas1to8DigitRange() {
        var codeGen = new CodeGenerator();
        var generatedCode = codeGen.generateCode();
        for(int i = 0; i < generatedCode.length(); i++) {
            int digit = Character.getNumericValue(generatedCode.charAt(i));
            assertTrue(digit >=1 && digit <= 8 );
        }
    }
}
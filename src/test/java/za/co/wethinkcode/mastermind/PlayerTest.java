package za.co.wethinkcode.mastermind;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;


public class PlayerTest {
    @Test
    public void initial12Turns() {
        var player = new Player();
        assertEquals(12, player.getPlayerTurns());
    }

    @Test
    public void canLoseAChance() {
        var player = new Player();
        int turns = player.getPlayerTurns();
        player.loseATurn();
        assertEquals(turns - 1, player.getPlayerTurns());
    }

    @Test
    public void hasNoMoreTurns() {
        var player = new Player();
        int turns = player.getPlayerTurns();
        for (int i = 0; i < turns; i++) {
            assertFalse(player.hasNoTurns());
            player.loseATurn();
        }
        assertTrue(player.hasNoTurns());
    }

    @Test
    public void noNegativeTurns() {
        var player = new Player();
        int turns = player.getPlayerTurns();
        for (int i = 0; i < turns + 1; i++)
            player.loseATurn();
        assertEquals(0, player.getPlayerTurns());
    }

    @Test
    public void testGetGuess() {
        byte[] inputStreamData = "6354\n".getBytes();
        InputStream inputStream = new ByteArrayInputStream(inputStreamData);

        Player player = new Player(inputStream);
        assertEquals("6354", player.getGuess());
    }
    @Test
    public void quitWithQuit() {
        byte[] inputStreamData = "quit\n".getBytes();
        InputStream inputStream = new ByteArrayInputStream(inputStreamData);

        var player = new Player(inputStream);
        assertEquals("quit", player.getGuess());
        assertTrue(player.playerWantsToQuit());
    }

    @Test
    public void quitWithExit() {
        byte[] inputStreamData = "exit\n".getBytes();
        InputStream inputStream = new ByteArrayInputStream(inputStreamData);

        var player = new Player(inputStream);
        assertEquals("exit", player.getGuess());
        assertTrue(player.playerWantsToQuit());
    }

    @Test
    public void testVerifyGuessIncorrect1() {
        byte[] inputStreamData = "34d\n".getBytes();
        InputStream inputStream = new ByteArrayInputStream(inputStreamData);

        var player = new Player(inputStream);
        var result = player.verifyGuess("34d");
        assertEquals("badCode", result);
    }

    @Test
    public void testVerifyGuessIncorrect2() {
        byte[] inputStreamData = "3058\n".getBytes();
        InputStream inputStream = new ByteArrayInputStream(inputStreamData);

        var player = new Player(inputStream);
        var result = player.verifyGuess("3058");
        assertEquals("badCode", result);
    }

    @Test
    public void testVerifyGuessIncorrect3() {
        byte[] inputStreamData = "7295\n".getBytes();
        InputStream inputStream = new ByteArrayInputStream(inputStreamData);

        var player = new Player(inputStream);
        var result = player.verifyGuess("7295");
        assertEquals("badCode", result);
    }

    @Test
    public void testVerifyGuessIncorrect4() {
        byte[] inputStreamData = "162\n".getBytes();
        InputStream inputStream = new ByteArrayInputStream(inputStreamData);

        var player = new Player(inputStream);
        var result = player.verifyGuess("162");
        assertEquals("badCode", result);
    }

    @Test
    public void testVerifyGuessIncorrect5() {
        byte[] inputStreamData = "518462\n".getBytes();
        InputStream inputStream = new ByteArrayInputStream(inputStreamData);

        var player = new Player(inputStream);
        var result = player.verifyGuess("518462");
        assertEquals("badCode", result);
    }

    @Test
    public void testVerifyGuessCorrect() {
        byte[] inputStreamData = "8462\n".getBytes();
        InputStream inputStream = new ByteArrayInputStream(inputStreamData);

        var player = new Player(inputStream);
        var result = player.verifyGuess("8462");
        assertEquals("8462", result);
    }
}

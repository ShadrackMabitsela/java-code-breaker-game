package za.co.wethinkcode.mastermind;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MastermindTest {
    @Test
    public void testCheckCorrectnessWin1() {
        var master = new Mastermind();
        String masterCode = "8625";
        String playerCode = "8625";
        String result = master.checkCodeCorrectness(playerCode, masterCode);
        assertEquals("Congratulations!", result);
    }
    @Test
    public void testCheckCorrectnessWin2() {
        var master = new Mastermind();
        String masterCode = "7728";
        String playerCode = "7728";
        String result = master.checkCodeCorrectness(playerCode, masterCode);
        assertEquals("Congratulations!", result);
    }

    @Test
    public void testCheckCorrectnessLose1() {
        var master = new Mastermind();
        String masterCode = "6535";
        String playerCode = "1483";
        String result = master.checkCodeCorrectness(playerCode, masterCode);
        assertEquals("", result);
    }

    @Test
    public void testCheckCorrectnessLose2() {
        var master = new Mastermind();
        String masterCode = "6535";
        String playerCode = "1565";
        String result = master.checkCodeCorrectness(playerCode, masterCode);
        assertEquals("", result);
    }

    @Test
    public void testCheckCorrectnessLose3() {
        var master = new Mastermind();
        String masterCode = "1953";
        String playerCode = "9513";
        String result = master.checkCodeCorrectness(playerCode, masterCode);
        assertEquals("", result);
    }
}
